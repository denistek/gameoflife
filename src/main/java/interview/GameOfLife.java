package interview;

import java.util.HashSet;
import java.util.Set;

public class GameOfLife {

	public final static int ROW_LIMIT = 6;
	public final static int COLUMN_LIMIT = 8;

	// User a number to represent only alive points. For example, 11 means: x = 1, y= 1. The second one on x axis and second one on y axis
	public Set<Integer> alivePoints = new HashSet<Integer>();
	public final static int[] SURROUNDING_NEIGHBORS = { -11, -10, -9, -1, 1, 9, 10, 11 };


	public int countLivingNeighbors(int index) {
		int result = 0;
		for (int neighbor : SURROUNDING_NEIGHBORS) {
			if (alivePoints.contains(index + neighbor))
				result++;
		}
		return result;
	}

	public GameOfLife nextGeneration() {
		System.out.println("exising board:");
		printBoard();
		GameOfLife nextGeneration = new GameOfLife();

		for (int i = 0; i < ROW_LIMIT; i++) {
			for (int j = 0; j < COLUMN_LIMIT; j++) {
				int point = i * 10 + j;
				if (countLivingNeighbors(point) == 3) {
					// no matter living or dead now, if the point has 3 neighbors, it always survives
					nextGeneration.alivePoints.add(point);
				} else if (alivePoints.contains(point) && countLivingNeighbors(point) == 2) {
					nextGeneration.alivePoints.add(point);
				}
			}
		}
		System.out.println("new board:");
		nextGeneration.printBoard();
		return nextGeneration;
	}

	/**
	 * Really a dummy one. The up-left corner is marked as 0. The right bottom is 57.
	 */
	public void printBoard() {
		for (int i = 0; i < ROW_LIMIT; i++) {
			for (int j = 0; j < COLUMN_LIMIT; j++) {
				if (alivePoints.contains(i * 10 + j)) {
					System.out.print("O");
				} else {
					System.out.print(".");
				}
			}
			System.out.println("");
		}

	}

}
