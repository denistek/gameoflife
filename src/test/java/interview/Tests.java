package interview;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class Tests {

	private GameOfLife game;

	@Before
	public void setupEmptyBoard() {
		game = new GameOfLife();
	}

	@Test
	public void emptyBoardNeighbors() {
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 8; j++) {
				assertEquals(game.countLivingNeighbors(i * 10 + j), 0);
			}
		}
	}

	@Test
	public void emptyBoardNextGeneration() {
		game = game.nextGeneration();
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 8; j++) {
				assertEquals(game.countLivingNeighbors(i * 10 + j), 0);
			}
		}
		assertEquals(game.alivePoints.size(), 0);
	}

	@Test
	public void onePointNeighbours() {
		game.alivePoints.add(12);

		assertEquals(game.countLivingNeighbors(1), 1);
		assertEquals(game.countLivingNeighbors(11), 1);
		assertEquals(game.countLivingNeighbors(21), 1);
		assertEquals(game.countLivingNeighbors(31), 0);
		assertEquals(game.countLivingNeighbors(2), 1);
		assertEquals(game.countLivingNeighbors(12), 0);
		assertEquals(game.countLivingNeighbors(22), 1);
		assertEquals(game.countLivingNeighbors(32), 0);
		assertEquals(game.countLivingNeighbors(3), 1);
		assertEquals(game.countLivingNeighbors(13), 1);
		assertEquals(game.countLivingNeighbors(23), 1);
		assertEquals(game.countLivingNeighbors(33), 0);
	}

	@Test
	public void onePointNextGeneration() {
		game.alivePoints.add(12);
		game = game.nextGeneration();
		for (int i = 0; i < GameOfLife.ROW_LIMIT; i++) {
			for (int j = 0; j < GameOfLife.COLUMN_LIMIT; j++) {
				assertEquals(game.countLivingNeighbors(i * 10 + j), 0);
			}
		}
		assertEquals(game.alivePoints.size(), 0);
	}

	@Test
	public void twoPointNeighbours() {

		game.alivePoints.add(23);
		game.alivePoints.add(24);

		assertEquals(game.countLivingNeighbors(2), 0);
		assertEquals(game.countLivingNeighbors(3), 0);
		assertEquals(game.countLivingNeighbors(4), 0);
		assertEquals(game.countLivingNeighbors(5), 0);
		assertEquals(game.countLivingNeighbors(6), 0);

		assertEquals(game.countLivingNeighbors(12), 1);
		assertEquals(game.countLivingNeighbors(13), 2);
		assertEquals(game.countLivingNeighbors(14), 2);
		assertEquals(game.countLivingNeighbors(15), 1);
		assertEquals(game.countLivingNeighbors(16), 0);

		assertEquals(game.countLivingNeighbors(22), 1);
		assertEquals(game.countLivingNeighbors(23), 1);
		assertEquals(game.countLivingNeighbors(24), 1);
		assertEquals(game.countLivingNeighbors(25), 1);
		assertEquals(game.countLivingNeighbors(26), 0);

		assertEquals(game.countLivingNeighbors(32), 1);
		assertEquals(game.countLivingNeighbors(33), 2);
		assertEquals(game.countLivingNeighbors(34), 2);
		assertEquals(game.countLivingNeighbors(35), 1);
		assertEquals(game.countLivingNeighbors(36), 0);

	}

	public void twoPointNextGeneration() {

		game.alivePoints.add(23);
		game.alivePoints.add(24);

		game = game.nextGeneration();
		for (int i = 0; i < 6; i++) {
			for (int j = 0; j < 8; j++) {
				assertEquals(game.countLivingNeighbors(i * 10 + j), 0);
			}
		}
		assertEquals(game.alivePoints.size(), 0);
	}

	@Test
	public void threePointInRowNextGeneration() {

		game.alivePoints.add(13);
		game.alivePoints.add(14);
		game.alivePoints.add(15);
		game = game.nextGeneration();
		assertFalse(game.alivePoints.contains(34));
		assertTrue(game.alivePoints.contains(4));
		assertTrue(game.alivePoints.contains(14));
		assertTrue(game.alivePoints.contains(24));
		assertEquals(game.alivePoints.size(), 3);
	}

	@Test
	public void threeColumnInRowNextGeneration() {

		game.alivePoints.add(6);
		game.alivePoints.add(16);
		game.alivePoints.add(26);
		game = game.nextGeneration();
		assertTrue(game.alivePoints.contains(15));
		assertTrue(game.alivePoints.contains(16));
		assertTrue(game.alivePoints.contains(17));
		assertEquals(game.alivePoints.size(), 3);
	}

	@Test
	public void squareNextGeneration() {

		game.alivePoints.add(3);
		game.alivePoints.add(4);
		game.alivePoints.add(13);
		game.alivePoints.add(14);
		game = game.nextGeneration();
		assertTrue(game.alivePoints.contains(3));
		assertTrue(game.alivePoints.contains(4));
		assertTrue(game.alivePoints.contains(13));
		assertTrue(game.alivePoints.contains(14));
		assertEquals(game.alivePoints.size(), 4);
	}

	@Test
	public void L3NextGeneration() {

		game.alivePoints.add(0);
		game.alivePoints.add(1);
		game.alivePoints.add(11);

		game = game.nextGeneration();
		assertTrue(game.alivePoints.contains(0));
		assertTrue(game.alivePoints.contains(1));
		assertTrue(game.alivePoints.contains(11));
		assertTrue(game.alivePoints.contains(10));
		assertEquals(game.alivePoints.size(), 4);
	}

	@Test
	public void L4ShapeNextGeneration() {

		game.alivePoints.add(3);
		game.alivePoints.add(4);
		game.alivePoints.add(5);
		game.alivePoints.add(15);

		game = game.nextGeneration();
		assertTrue(game.alivePoints.contains(4));
		assertTrue(game.alivePoints.contains(5));
		assertTrue(game.alivePoints.contains(15));
		assertEquals(game.alivePoints.size(), 3);
	}

	@Test
	public void L5ShapeNextGeneration() {

		game.alivePoints.add(3);
		game.alivePoints.add(4);
		game.alivePoints.add(5);
		game.alivePoints.add(15);
		game.alivePoints.add(25);

		game = game.nextGeneration();
		assertTrue(game.alivePoints.contains(4));
		assertTrue(game.alivePoints.contains(5));
		assertTrue(game.alivePoints.contains(15));
		assertTrue(game.alivePoints.contains(16));
		assertEquals(game.alivePoints.size(), 4);
	}

	@Test
	public void pointDiesOnOverPopulation() {

		game.alivePoints.add(14);
		game.alivePoints.add(03);
		game.alivePoints.add(04);
		game.alivePoints.add(05);
		game.alivePoints.add(15);

		game = game.nextGeneration();

		assertTrue(game.alivePoints.contains(3));
		assertTrue(game.alivePoints.contains(5));
		assertTrue(game.alivePoints.contains(13));
		assertTrue(game.alivePoints.contains(15));
		assertEquals(game.alivePoints.size(), 4);

	}

	@Test
	public void aRandomShape() {

		game.alivePoints.add(01);
		game.alivePoints.add(10);
		game.alivePoints.add(21);

		game = game.nextGeneration();
		assertTrue(game.alivePoints.contains(10));
		assertTrue(game.alivePoints.contains(11));
		assertEquals(game.alivePoints.size(), 2);
	}

	@Test
	public void overallTest() {

		game.alivePoints.add(6);
		game.alivePoints.add(16);
		game.alivePoints.add(26);

		game.alivePoints.add(10);
		game.alivePoints.add(11);
		game.alivePoints.add(12);

		game.alivePoints.add(43);
		game.alivePoints.add(44);
		game.alivePoints.add(53);
		game.alivePoints.add(54);

		game = game.nextGeneration();
		assertTrue(game.alivePoints.contains(1));
		assertTrue(game.alivePoints.contains(11));
		assertTrue(game.alivePoints.contains(21));

		assertTrue(game.alivePoints.contains(15));
		assertTrue(game.alivePoints.contains(16));
		assertTrue(game.alivePoints.contains(17));

		assertTrue(game.alivePoints.contains(43));
		assertTrue(game.alivePoints.contains(44));
		assertTrue(game.alivePoints.contains(53));
		assertTrue(game.alivePoints.contains(54));

		assertEquals(game.alivePoints.size(), 10);
	}
}
